var request = require('request');
var Trakt = require('trakt-api');
var trakt = Trakt('e484a4b9962d2618c7916b088d0cbaa529481b372d44ebcfa672e7749333a8a9' ,[]);

// // Variables
// var traktConfigs = {
//     imdbId: 'tt4158110',
//     season: 1,
//     episode: 1,
//     extended: 'min'
// }

// // get show...
// trakt
//     .show(traktConfigs.imdbId, { extended : traktConfigs.extended })
//         .then(function(show) {
//             console.log('%j', show);
//         })
//         .catch(function(err) {
//             console.warn('oh noes', err);
//         });

// // get show comments...
// trakt
//     .showComments(traktConfigs.imdbId, { extended : traktConfigs.extended })
//         .then(function(comments) {
//             console.log('%j', comments);
//         })
//         .catch(function(err) {
//             console.warn('oh noes', err);
//         });

// // show watching...
// trakt
//     .episodeWatching(traktConfigs.imdbId, traktConfigs.season, traktConfigs.episode, { extended : traktConfigs.extended })
//         .then(function(watching) {
//             console.log('%j', watching);
//         })
//         .catch(function(err) {
//             console.warn('oh noes', err);
//         });



// Request for access token

// request({
//     method: 'POST',
//     url: 'https://private-anon-1c7f14c46-trakt.apiary-mock.com/oauth/token',
//     headers: {
//         'Content-Type': 'application/json'
//     },
//     body: "{\"code\": \"ba8c25ec295b087347e3d334e451dc4f1d53179fb9bdbd2ac1b62f8ad2af41c3\", \"client_id\": \"e484a4b9962d2618c7916b088d0cbaa529481b372d44ebcfa672e7749333a8a9\", \"client_secret\": \"2df9745498d27a7227c44f8ec4080bf41cacaee9bf71257310e5e85261ede94a\", \"redirect_uri\": \"urn:ietf:wg:oauth:2.0:oob\", \"grant_type\": \"authorization_code\"}"}, function (error, response, body) {
//     console.log('Status:', response.statusCode);
//     console.log('Headers:', JSON.stringify(response.headers));
//     console.log('Response:', body);
// });

// Custom Checkin magic is startin' heree

request({
  method: 'POST',
  url: 'https://private-anon-1c7f14c46-trakt.apiary-mock.com/checkin',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': 'dbaf9757982a9e738f05d249b7b5b4a266b3a139049317c4909f2f263572c781',
    'trakt-api-version': '2',
    'trakt-api-key': 'e484a4b9962d2618c7916b088d0cbaa529481b372d44ebcfa672e7749333a8a9'
  },
  body: "{  \"movie\": {    \"title\": \"Guardians of the Galaxy\",    \"year\": 2014,    \"ids\": {      \"trakt\": 28,      \"slug\": \"guardians-of-the-galaxy-2014\",      \"imdb\": \"tt2015381\",      \"tmdb\": 118340    }  },  \"sharing\": {    \"facebook\": true,    \"twitter\": true,    \"tumblr\": false  },  \"message\": \"Guardians of the Galaxy FTW!\",  \"app_version\": \"1.0\",  \"app_date\": \"2014-09-22\"}"
}, function (error, response, body) {
  console.log('Status:', response.statusCode);
  console.log('Headers:', JSON.stringify(response.headers));
  console.log('Response:', body);
});
